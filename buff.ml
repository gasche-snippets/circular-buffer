type 'a circular_buffer = {
  mutable pos : int;
  mutable count : int;
  data : 'a array;
  size : int;
}

let create size dummy = {
  pos = 0;
  count = 0;
  data = Array.make size dummy;
  size;
}

let push buffer elem =
  let k = buffer.pos + buffer.count in
  let k = if k < buffer.size then k else k - buffer.size in
  buffer.data.(k) <- elem;
  if buffer.count < buffer.size then
    buffer.count <- buffer.count + 1
  else 
    buffer.pos <- buffer.pos + 1;
  ()

let pop buffer =
  if buffer.count = 0 then raise Not_found;
  let result = buffer.data.(buffer.pos) in
  (* if you want to free the buffer content, buffer.data.(pos) <- dummy *)
  let pos' = buffer.pos + 1 in
  buffer.pos <- (if pos' < buffer.size then pos' else 0);
  buffer.count <- buffer.count - 1;
  result

(* test *)
let () =
  let buf = create 2 '0' in
  (* *)
  push buf '1';
  (* 1 *)
  push buf '2';
  (* 1 2 *)
  push buf '3';
  (*   2 3 *)
  assert (pop buf = '2');
  (*     3 *)
  push buf '4';
  (*     3 4 *)
  assert (pop buf = '3');
  (*       4 *)
  assert (pop buf = '4');
  (*         *)
  assert (try ignore (pop buf); false with Not_found -> true);
  assert (try ignore (pop buf); false with Not_found -> true);
